package com.deep.tmdb.dao.api;

import com.deep.tmdb.dao.models.api.MovieResponse;
import com.deep.tmdb.dao.models.local.MovieDetail;

import java.util.Date;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Movies {

    String subPath = "movie/";
    String subPath2 = "discover/movie";

    @GET(subPath + "{type}")
    Call<MovieResponse> get(@Path("type") String type, @Query("page") int page);

    @GET(subPath + "{id}")
    Call<MovieDetail> getOne(@Path("id") int id);

    @GET(subPath2)
    Call<MovieResponse> get(@Query("primary_release_date.gte") String date1, @Query("primary_release_date.lte") String date2, @Query("page") int page);

}
