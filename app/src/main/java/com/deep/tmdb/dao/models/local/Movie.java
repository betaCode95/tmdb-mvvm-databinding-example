package com.deep.tmdb.dao.models.local;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "movies")
public class Movie {

    @ColumnInfo(name = "vote_count")
    private int vote_count;

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "video")
    private boolean video;

    @ColumnInfo(name = "vote_average")
    private float vote_average;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "popularity")
    private float popularity;

    @ColumnInfo(name = "poster_path")
    private String poster_path;

    @ColumnInfo(name = "original_language")
    private String original_language;

    @ColumnInfo(name = "original_title")
    private String original_title;

    @ColumnInfo(name = "adult")
    private boolean adult;

    @ColumnInfo(name = "overview")
    private String overview;

    @ColumnInfo(name = "release_date")
    private String release_date;


    @ColumnInfo(name = "backdrop_path")
    private String backdrop_path;

    public int getVote_count() {
        return vote_count;
    }

    public int getId() {
        return id;
    }

    public boolean isVideo() {
        return video;
    }

    public float getVote_average() {
        return vote_average;
    }

    public String getTitle() {
        return title;
    }

    public float getPopularity() {
        return popularity;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public String getOriginal_title() {
        return original_title;
    }

    public boolean isAdult() {
        return adult;
    }

    public String getOverview() {
        return overview;
    }

    public String getRelease_date() {
        return release_date;
    }

    public Date getReleaseDate() {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
            return format.parse(release_date);
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            return null;
        }
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @BindingAdapter("imageUrl")
    public static void load(ImageView view, String imageUrl) {
        Glide.with(view.getContext())
                .load("http://image.tmdb.org/t/p/w500" + imageUrl)
                .into(view);
    }
}
