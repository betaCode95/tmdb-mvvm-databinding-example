package com.deep.tmdb.dao.local;

import com.deep.tmdb.dao.models.local.Movie;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Query;

@Dao
public interface Movies {

    @Query("select * from movies")
    List<Movie> movies();
}
