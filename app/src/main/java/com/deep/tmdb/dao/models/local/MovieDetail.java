package com.deep.tmdb.dao.models.local;

public class MovieDetail extends Movie {

    private long revenue;
    private String tagline;
    private String status;

    public class Genre {
        public String name;
    }

    public long getRevenue() {
        return revenue;
    }

    public String getTagline() {
        return tagline;
    }

    public String getStatus() {
        return status;
    }
}
