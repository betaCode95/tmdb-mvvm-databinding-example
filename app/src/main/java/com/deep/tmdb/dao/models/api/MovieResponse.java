package com.deep.tmdb.dao.models.api;


import com.deep.tmdb.dao.models.local.Movie;

import java.util.ArrayList;
import java.util.List;

public class MovieResponse {
    public int page;
    public int total_pages;
    public int total_results;
    public ArrayList<Movie> results;
}
