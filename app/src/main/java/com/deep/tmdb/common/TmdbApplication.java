package com.deep.tmdb.common;

import android.app.Application;

import com.deep.tmdb.di.components.AppComponent;
import com.deep.tmdb.di.components.DaggerAppComponent;
import com.deep.tmdb.di.modules.AppModule;

public class TmdbApplication extends Application {

    private AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        component = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        component.inject(this);
    }

    public AppComponent getAppComponent() {
        return component;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }
}
