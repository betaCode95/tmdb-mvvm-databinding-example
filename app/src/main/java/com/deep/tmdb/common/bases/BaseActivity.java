package com.deep.tmdb.common.bases;

import android.os.Bundle;

import com.deep.tmdb.common.TmdbApplication;
import com.deep.tmdb.di.components.AppComponent;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        resolveDaggerDependancy();
        onViewReady(savedInstanceState);
    }

    public abstract void onViewReady(@Nullable Bundle savedInstanceState);

    public abstract int getContentView();

    public abstract void resolveDaggerDependancy();

    protected AppComponent getAppComponent() {
        return ((TmdbApplication) getApplication()).getAppComponent();
    }
}
