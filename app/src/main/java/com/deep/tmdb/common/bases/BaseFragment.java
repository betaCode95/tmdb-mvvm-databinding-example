package com.deep.tmdb.common.bases;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.deep.tmdb.common.TmdbApplication;
import com.deep.tmdb.di.components.AppComponent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public abstract class BaseFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        resolveDagger();
        return setView(inflater, container, savedInstanceState);
    }

    protected abstract int getContentView();

    protected abstract View setView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);

    protected abstract void resolveDagger();

    protected AppComponent getAppComponent() {
        return ((TmdbApplication) getActivity().getApplication()).getAppComponent();
    }

    @Override
    public void onAttach(Context context) {
        resolveDagger();
        super.onAttach(context);
    }
}
