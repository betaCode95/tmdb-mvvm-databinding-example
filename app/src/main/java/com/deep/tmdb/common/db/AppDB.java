package com.deep.tmdb.common.db;

import com.deep.tmdb.dao.local.Movies;
import com.deep.tmdb.dao.models.local.Movie;

import androidx.room.Database;
import androidx.room.RoomDatabase;


@Database(entities = {Movie.class}, version = 2)
public abstract class AppDB extends RoomDatabase {

    public abstract Movies movies();
}
