package com.deep.tmdb.features.detail.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.deep.tmdb.R;
import com.deep.tmdb.common.bases.BaseActivity;
import com.deep.tmdb.dao.models.local.Movie;
import com.deep.tmdb.dao.models.local.MovieDetail;
import com.deep.tmdb.databinding.ActivityDetailBinding;
import com.deep.tmdb.features.detail.viewmodel.DetailViewModel;
import com.deep.tmdb.features.detail.viewmodel.callbacks.DetailCallbacks;
import com.deep.tmdb.features.detail.viewmodel.factory.DetailViewModelFactory;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import io.reactivex.annotations.Nullable;
import retrofit2.Retrofit;


public class DetailActivity extends BaseActivity implements DetailCallbacks {

    @Inject
    Retrofit retrofit;

    private ActivityDetailBinding binding;
    private DetailViewModel model;

    public static void open(Context context, int id) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra("id", id);
        context.startActivity(intent);
    }

    @Override
    public void resolveDaggerDependancy() {
        getAppComponent().inject(this);
    }

    @Override
    public int getContentView() {
        return R.layout.activity_detail;
    }

    @Override
    public void onViewReady(@Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, getContentView());
        binding.setViewModel(ViewModelProviders.of(this, new DetailViewModelFactory(this, retrofit)).get(DetailViewModel.class));
        binding.status.setText("Loading..");
        model = binding.getViewModel();
        model.setIntent(getIntent());
        model.get();
    }

    @Override
    public void setMovieDetail(MovieDetail m) {
        binding.setMovie(m);
    }

    @Override
    public void setToolbar() {
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
