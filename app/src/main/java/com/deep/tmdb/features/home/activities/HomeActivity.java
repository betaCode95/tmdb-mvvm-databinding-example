package com.deep.tmdb.features.home.activities;

import android.os.Bundle;
import android.view.Menu;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import retrofit2.Retrofit;

import com.deep.tmdb.R;
import com.deep.tmdb.common.bases.BaseActivity;
import com.deep.tmdb.dao.api.Movies;
import com.deep.tmdb.databinding.ActivityHomeBinding;
import com.deep.tmdb.features.home.adapters.HomePagerAdapter;
import com.deep.tmdb.features.home.viewmodel.HomeViewModel;
import com.deep.tmdb.features.home.viewmodel.callbacks.HomeResultCallbacks;
import com.deep.tmdb.features.home.viewmodel.factory.HomeViewModelFactory;

import javax.inject.Inject;

public class HomeActivity extends BaseActivity implements HomeResultCallbacks {

    private ActivityHomeBinding binding;
    private HomeViewModel model;

    @Override
    public int getContentView() {
        return R.layout.activity_home;
    }

    @Override
    public void resolveDaggerDependancy() {

    }

    @Override
    public void onViewReady(@Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, getContentView());
        binding.setViewModel(ViewModelProviders.of(this, new HomeViewModelFactory(this)).get(HomeViewModel.class));
        model = binding.getViewModel();
    }

    @Override
    public void setToolbar() {
        binding.toolbar.setTitle("TMDB");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void setFragments() {
        binding.viewPager.setAdapter(new HomePagerAdapter(getSupportFragmentManager()));
        binding.tabLayout.setupWithViewPager(binding.viewPager);
    }
}
