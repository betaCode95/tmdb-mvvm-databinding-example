package com.deep.tmdb.features.home.viewmodel;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.deep.tmdb.dao.api.Movies;
import com.deep.tmdb.dao.models.api.MovieResponse;
import com.deep.tmdb.features.home.viewmodel.callbacks.MovieListCallbacks;

import javax.inject.Inject;

import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MovieListViewModel extends ViewModel {

    @Inject
    Retrofit retrofit;

    private Movies movies;
    private int page;
    private int total;
    private MovieListCallbacks callbacks;
    private boolean filtered;
    private String startDate, endDate;

    public enum Types {
        POPULAR("popular"), TOP_RATED("top_rated");

        private String type;

        Types(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    public MovieListViewModel(Retrofit retrofit, MovieListCallbacks callbacks) {
        this.callbacks = callbacks;
        this.retrofit = retrofit;
        movies = retrofit.create(Movies.class);
        init();
    }

    public void init() {
        page = 1;
        startDate = "";
        startDate = "";
        filtered = false;
        get(Types.POPULAR);
    }

    public void get(Types type) {
        Call<MovieResponse> call = movies.get(type.getType(), page);
        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response != null && response.body() != null) {
                    total = response.body().total_pages;
                    callbacks.setMovies(response.body().results);
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                Log.e("Error", t.toString());
            }
        });
    }

    public void setFiltered(String date1, String date2) {
        page = 1;
        filtered = true;
        startDate = date1;
        endDate = date2;
    }

    public void getWithDates(String date1, String date2) {
        Call<MovieResponse> call = movies.get(date1, date2, page);
        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response != null && response.body() != null) {
                    total = response.body().total_pages;
                    callbacks.setMovies(response.body().results);
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                Log.e("Error", t.toString());
            }
        });
    }

    public void loadNextPage() {
        if (page < total) {
            page++;
            if (!filtered)
                get(Types.POPULAR);
            else
                getWithDates(startDate, endDate);
        }
    }

    public void showFilter(View v) {
        callbacks.showFilter();
    }

    public void pickStartDate(View v) {
        callbacks.pickStartDate();
    }

    public void pickEndDate(View v) {
        callbacks.pickEndDate();
    }

    public void applyFilter(View v) {
        if (((TextView) v).getText().toString().equalsIgnoreCase("Apply"))
            callbacks.applyFilters();
        else
            callbacks.clearFilters();
    }

}
