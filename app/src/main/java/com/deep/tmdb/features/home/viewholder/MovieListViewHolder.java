package com.deep.tmdb.features.home.viewholder;

import android.view.View;

import com.deep.tmdb.dao.models.local.Movie;
import com.deep.tmdb.databinding.ItemCardBinding;
import com.deep.tmdb.features.detail.activities.DetailActivity;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class MovieListViewHolder extends RecyclerView.ViewHolder {

    private ItemCardBinding binding;

    public MovieListViewHolder(@NonNull ItemCardBinding binding) {
        super(binding.getRoot());
        binding = DataBindingUtil.getBinding(itemView);
        binding.setViewModel(this);
        this.binding = binding;
    }

    public void set(Movie movie, int position) {
        binding.setMovie(movie);
    }

    public void open(View view, Movie movie) {
        DetailActivity.open(binding.getRoot().getContext(), movie.getId());
    }

}
