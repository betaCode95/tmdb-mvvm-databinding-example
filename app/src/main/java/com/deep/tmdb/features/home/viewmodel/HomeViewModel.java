package com.deep.tmdb.features.home.viewmodel;

import com.deep.tmdb.dao.api.Movies;
import com.deep.tmdb.dao.models.api.MovieResponse;
import com.deep.tmdb.features.home.viewmodel.callbacks.HomeResultCallbacks;

import javax.inject.Inject;

import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class HomeViewModel extends ViewModel {

    private HomeResultCallbacks callbacks;

//    public enum Types {
//        POPULAR("popular"), TOP_RATED("top_rated");
//
//        private String type;
//
//        Types(String type) {
//            this.type = type;
//        }
//
//        public String getType() {
//            return type;
//        }
//    }

    public HomeViewModel(HomeResultCallbacks callbacks) {
        this.callbacks = callbacks;
        init();
    }

    private void init() {
        this.callbacks.setToolbar();
        this.callbacks.setFragments();
    }

//    public void get(Types type) {
//        Call<MovieResponse> movies = movieApi.get(type.getType());
//        movies.enqueue(new Callback<MovieResponse>() {
//            @Override
//            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<MovieResponse> call, Throwable t) {
//
//            }
//        });
//    }

}
