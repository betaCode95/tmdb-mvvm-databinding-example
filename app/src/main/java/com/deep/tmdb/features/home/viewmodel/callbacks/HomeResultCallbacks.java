package com.deep.tmdb.features.home.viewmodel.callbacks;

public interface HomeResultCallbacks {

    void setFragments();

    void setToolbar();
}
