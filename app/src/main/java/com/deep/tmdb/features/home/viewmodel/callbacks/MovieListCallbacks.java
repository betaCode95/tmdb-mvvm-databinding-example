package com.deep.tmdb.features.home.viewmodel.callbacks;

import com.deep.tmdb.dao.models.local.Movie;

import java.util.ArrayList;

public interface MovieListCallbacks {

    void setMovies(ArrayList<Movie> movies);

    void loadNext();

    void showFilter();

    void pickStartDate();

    void pickEndDate();

    void applyFilters();

    void clearFilters();
}
