package com.deep.tmdb.features.home.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.deep.tmdb.R;
import com.deep.tmdb.common.bases.BaseFragment;
import com.deep.tmdb.databinding.FragSettingsBinding;
import com.deep.tmdb.features.home.viewmodel.SettingViewModel;
import com.deep.tmdb.features.home.viewmodel.callbacks.SettingsCallbacks;
import com.deep.tmdb.features.home.viewmodel.factory.SettingsListViewModelFactory;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

public class SettingsFragment extends BaseFragment implements SettingsCallbacks {

    private FragSettingsBinding binding;
    private SettingViewModel model;

    @Override
    protected int getContentView() {
        return R.layout.frag_settings;
    }

    @Override
    protected void resolveDagger() {
        getAppComponent().inject(this);
    }

    @Override
    protected View setView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getContentView(), container, false);
        View v = binding.getRoot();
        binding.setViewModel(ViewModelProviders.of(this, new SettingsListViewModelFactory(this)).get(SettingViewModel.class));
        model = binding.getViewModel();
        return v;
    }
}
