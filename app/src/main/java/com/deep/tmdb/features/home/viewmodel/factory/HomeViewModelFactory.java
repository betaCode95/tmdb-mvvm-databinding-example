package com.deep.tmdb.features.home.viewmodel.factory;

import com.deep.tmdb.features.home.viewmodel.HomeViewModel;
import com.deep.tmdb.features.home.viewmodel.callbacks.HomeResultCallbacks;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class HomeViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private HomeResultCallbacks callbacks;

    public HomeViewModelFactory(HomeResultCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new HomeViewModel(callbacks);
    }
}
