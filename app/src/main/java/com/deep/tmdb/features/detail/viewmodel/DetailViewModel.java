package com.deep.tmdb.features.detail.viewmodel;

import android.content.Intent;
import android.os.Bundle;

import com.deep.tmdb.dao.api.Movies;
import com.deep.tmdb.dao.models.local.MovieDetail;
import com.deep.tmdb.features.detail.viewmodel.callbacks.DetailCallbacks;

import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DetailViewModel extends ViewModel {

    private DetailCallbacks callbacks;
    private Retrofit retrofit;
    private int id;
    private Movies movies;

    public DetailViewModel(DetailCallbacks callbacks, Retrofit retrofit) {
        this.callbacks = callbacks;
        this.retrofit = retrofit;
    }

    public void setIntent(Intent intent) {
        if (intent != null && intent.getExtras() != null)
            id = intent.getExtras().getInt("id");
        callbacks.setToolbar();
    }

    public void get() {
        this.movies = retrofit.create(Movies.class);
        Call<MovieDetail> movie = movies.getOne(id);
        movie.enqueue(new Callback<MovieDetail>() {
            @Override
            public void onResponse(Call<MovieDetail> call, Response<MovieDetail> response) {
                if (response != null && response.body() != null)
                    callbacks.setMovieDetail(response.body());
            }

            @Override
            public void onFailure(Call<MovieDetail> call, Throwable t) {

            }
        });
    }
}
