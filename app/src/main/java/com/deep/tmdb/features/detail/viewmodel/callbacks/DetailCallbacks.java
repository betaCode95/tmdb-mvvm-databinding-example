package com.deep.tmdb.features.detail.viewmodel.callbacks;

import com.deep.tmdb.dao.models.local.MovieDetail;

public interface DetailCallbacks {

    void setMovieDetail(MovieDetail m);

    void setToolbar();
}
