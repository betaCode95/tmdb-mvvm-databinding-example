package com.deep.tmdb.features.home.adapters;

import com.deep.tmdb.features.home.fragments.ListFragment;
import com.deep.tmdb.features.home.fragments.SettingsFragment;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class HomePagerAdapter extends FragmentStatePagerAdapter {

    public HomePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ListFragment();
            case 1:
                return new SettingsFragment();
            default:
                return new ListFragment();
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Movies";
            case 1:
                return "Settings";
            default:
                return "Movies";
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
