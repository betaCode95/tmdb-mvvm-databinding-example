package com.deep.tmdb.features.home.fragments;

import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.deep.tmdb.R;
import com.deep.tmdb.common.bases.BaseFragment;
import com.deep.tmdb.dao.models.local.Movie;
import com.deep.tmdb.databinding.FragListBinding;
import com.deep.tmdb.features.home.adapters.MovieListAdapter;
import com.deep.tmdb.features.home.viewmodel.MovieListViewModel;
import com.deep.tmdb.features.home.viewmodel.callbacks.MovieListCallbacks;
import com.deep.tmdb.features.home.viewmodel.factory.MovieListViewModelFactory;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManagerNonConfig;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import retrofit2.Retrofit;

public class ListFragment extends BaseFragment implements MovieListCallbacks {

    @Inject
    Retrofit retrofit;

    private FragListBinding binding;
    private MovieListViewModel model;
    private MovieListAdapter adapter;

    @Override
    protected int getContentView() {
        return R.layout.frag_list;
    }

    @Override
    protected void resolveDagger() {
        getAppComponent().inject(this);
    }

    @Override
    protected View setView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getContentView(), container, false);
        View v = binding.getRoot();
        binding.setViewModel(ViewModelProviders.of(this, new MovieListViewModelFactory(retrofit, this)).get(MovieListViewModel.class));
        binding.backDrop.setVisibility(View.GONE);
        binding.filterView.setVisibility(View.GONE);
        model = binding.getViewModel();
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_filter, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void setMovies(ArrayList<Movie> movies) {
        if (adapter == null) {
            binding.recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
            adapter = new MovieListAdapter(movies, this);
            binding.recycler.setAdapter(adapter);
        } else {
            adapter.update(movies);
        }
    }

    @Override
    public void loadNext() {
        model.loadNextPage();
    }

    @Override
    public void showFilter() {
        binding.backDrop.setVisibility(View.VISIBLE);
        binding.filterView.setVisibility(View.VISIBLE);
        if (binding.startDate.getText().toString().equalsIgnoreCase("DATE") ||
                binding.endDate.getText().toString().equalsIgnoreCase("DATE")) {
            binding.apply.setText("Apply");
        } else {
            binding.apply.setText("Clear Filters");
        }
    }

    @Override
    public void pickStartDate() {
        Calendar cur_calender = Calendar.getInstance();
        DatePickerDialog datePicker = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        String date = year + "-" + monthOfYear + "-" + dayOfMonth;
                        binding.startDate.setText(date);
                    }
                },
                cur_calender.get(Calendar.YEAR),
                cur_calender.get(Calendar.MONTH),
                cur_calender.get(Calendar.DAY_OF_MONTH)
        );
        datePicker.setThemeDark(true);
        datePicker.setAccentColor(getResources().getColor(R.color.grey_60));
        datePicker.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void pickEndDate() {
        Calendar cur_calender = Calendar.getInstance();
        DatePickerDialog datePicker = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        String date = year + "-" + monthOfYear + "-" + dayOfMonth;
                        binding.endDate.setText(date);
                    }
                },
                cur_calender.get(Calendar.YEAR),
                cur_calender.get(Calendar.MONTH),
                cur_calender.get(Calendar.DAY_OF_MONTH)
        );
        datePicker.setThemeDark(true);
        datePicker.setAccentColor(getResources().getColor(R.color.grey_60));
        datePicker.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void applyFilters() {
        adapter = null;
        model.setFiltered(binding.startDate.getText().toString(), binding.endDate.getText().toString());
        model.getWithDates(binding.startDate.getText().toString(), binding.endDate.getText().toString());
        binding.backDrop.setVisibility(View.GONE);
        binding.filterView.setVisibility(View.GONE);
    }

    @Override
    public void clearFilters() {
        adapter = null;
        model.init();
        binding.apply.setText("Apply");
        binding.startDate.setText("DATE");
        binding.endDate.setText("DATE");
        binding.backDrop.setVisibility(View.GONE);
        binding.filterView.setVisibility(View.GONE);
    }
}
