package com.deep.tmdb.features.home.viewmodel;

import com.deep.tmdb.features.home.viewmodel.callbacks.SettingsCallbacks;

import androidx.lifecycle.ViewModel;

public class SettingViewModel extends ViewModel {

    private SettingsCallbacks callbacks;

    public SettingViewModel(SettingsCallbacks callbacks) {
        this.callbacks = callbacks;
    }
}
