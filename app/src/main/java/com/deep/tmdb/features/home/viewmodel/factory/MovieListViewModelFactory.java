package com.deep.tmdb.features.home.viewmodel.factory;

import com.deep.tmdb.features.home.viewmodel.MovieListViewModel;
import com.deep.tmdb.features.home.viewmodel.callbacks.MovieListCallbacks;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import retrofit2.Retrofit;

public class MovieListViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private MovieListCallbacks callbacks;
    private Retrofit retrofit;

    public MovieListViewModelFactory(Retrofit retrofit, MovieListCallbacks callbacks) {
        this.callbacks = callbacks;
        this.retrofit = retrofit;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MovieListViewModel(retrofit, callbacks);
    }
}
