package com.deep.tmdb.features.home.viewmodel.factory;

import com.deep.tmdb.features.home.viewmodel.SettingViewModel;
import com.deep.tmdb.features.home.viewmodel.callbacks.SettingsCallbacks;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class SettingsListViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private SettingsCallbacks callbacks;

    public SettingsListViewModelFactory(SettingsCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new SettingViewModel(callbacks);
    }
}
