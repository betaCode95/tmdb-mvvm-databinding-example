package com.deep.tmdb.features.home.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.deep.tmdb.R;
import com.deep.tmdb.dao.models.local.Movie;
import com.deep.tmdb.databinding.ItemCardBinding;
import com.deep.tmdb.features.home.viewholder.MovieListViewHolder;
import com.deep.tmdb.features.home.viewmodel.callbacks.MovieListCallbacks;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class MovieListAdapter extends RecyclerView.Adapter {

    private ArrayList<Movie> movies;
    private MovieListCallbacks callbacks;

    public MovieListAdapter(ArrayList<Movie> movies, MovieListCallbacks callbacks) {
        this.movies = movies;
        this.callbacks = callbacks;
    }

    public void update(ArrayList<Movie> movies) {
        this.movies.remove(movies.size() - 1);
        this.movies.addAll(movies);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemCardBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_card, parent, false);
        holder = new MovieListViewHolder(binding);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MovieListViewHolder) {
            if (position < getItemCount() - 1)
                ((MovieListViewHolder) holder).set(movies.get(position), position);
            else
                ((MovieListViewHolder) holder).set(null, position);
        }
        if (position == getItemCount() - 5)
            callbacks.loadNext();
    }

    @Override
    public int getItemCount() {
        return movies.size() + 1;
    }
}
