package com.deep.tmdb.features.detail.viewmodel.factory;

import com.deep.tmdb.features.detail.viewmodel.DetailViewModel;
import com.deep.tmdb.features.detail.viewmodel.callbacks.DetailCallbacks;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import retrofit2.Retrofit;

public class DetailViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private DetailCallbacks callbacks;
    private Retrofit retrofit;

    public DetailViewModelFactory(DetailCallbacks callbacks, Retrofit retrofit) {
        this.callbacks = callbacks;
        this.retrofit = retrofit;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new DetailViewModel(callbacks, retrofit);
    }
}
