package com.deep.tmdb.di.components;

import com.deep.tmdb.common.TmdbApplication;
import com.deep.tmdb.di.modules.AppModule;
import com.deep.tmdb.features.detail.activities.DetailActivity;
import com.deep.tmdb.features.home.activities.HomeActivity;
import com.deep.tmdb.features.home.fragments.ListFragment;
import com.deep.tmdb.features.home.fragments.SettingsFragment;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(TmdbApplication application);

    void inject(HomeActivity activity);

    void inject(DetailActivity activity);

    void inject(ListFragment fragment);

    void inject(SettingsFragment fragment);

}
